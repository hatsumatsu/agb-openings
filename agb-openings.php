<?php

/*
Plugin Name: AGB Openings
Version: 0.14
Description: Manage opening events directly on the edit exhibition screen
Plugin URI:
Author: Martin Wecke
Author URI: http://martinwecke.de/
Bitbucket Plugin URI: https://bitbucket.org/hatsumatsu/agb-openings/
Bitbucket Branch: master
*/


class AGBOpenings {
	protected $settings;

	public function __construct() {
		// i11n
		add_action( 'init', array( $this, 'loadI88n' ) );

		// load settings
		add_action( 'after_setup_theme', array( $this, 'loadSettings' ) );

		// cache MPL API
		add_action( 'inpsyde_mlp_loaded', array( $this, 'cacheMLPAPI' ) );		

		// attach post saving action 
		// use `save_post` to include ACF fields which are available later than `publish_post` 
		add_action( 'save_post', array( $this, 'savePost' ), 100, 2 );	

		// attach hm-network-auto-post meta (relations) action 
		// we need that to because the opening meta data is saved after the save_post action
		add_action( 'hmnap/set_meta', array( $this, 'HMNAPsetMeta' ), 100, 4 );					
		add_action( 'hmnap/set_meta_relations', array( $this, 'HMNAPsetMeta' ), 100, 4 );					

		// default settings
		$this->settings = array(
			'post_type' => array(
				'exhibitions' => 'exhibitions',
				'events'	  => 'events',
				'satelite'	  => 'satelite'
			),
			'meta_key' => array(
				'opening-date' 			=> 'opening--date',
				'opening-welcome'		=> 'opening--welcome',
				'opening-introduction'	=> 'opening--introduction',
				'opening-info'			=> 'opening--info',

				'hm-attachments'		=> 'hm-attachment',

				'show-on-home'			=> 'show-on-home',

				'satellite--is-satellite' => 'satellite--is-satellite'
			),
			'meta_key_relation' => array(
				'opening'	 => 'posts--exhibition',
				'exhibition' => 'opening--id'
			),
			'taxonomies' => array(
				'event_categories' => array(
					'ausstellungseroeffnung',
					'eroeffnung',
					'opening'
				)
			)
		);
	}


	/**
	 * Load i88n
	 */
	public function loadI88n() {
		load_plugin_textdomain( 'agb-openings', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );	
	}


	/**
	 * Load settings from filter 'hmnap/settings'
	 */
	public function loadSettings() {
		$this->settings = apply_filters( 'agbo/settings', $this->settings );
	}


	/**
	 * Cache MultilingualPress API
	 */
	public function cacheMLPAPI( $data ) {
		$this->mpl_api_cache = $data;
	}	


	/**
	 * Action to call when posts are saved
	 * @param int $source_post_id Source post ID
	 * @param WP_Post $post Source post
	 * @param bool $update Whether this is an existing post being updated or not.
	 */
	public function savePost( $source_post_id, $source_post ) {
		$this->writeLog( 'savePost()' );
		$this->writeLog( $source_post_id );


		// quit if post is post revisions
		if( wp_is_post_revision( $source_post_id ) ) {
			$this->writeLog( 'Ignore post revision...' );
			return;
		}

		// quit if post is trash or auto draft
		if( $source_post->post_status == 'trash' || $source_post->post_status == 'draft' || $source_post->post_status == 'auto-draft' ) {
			$this->writeLog( 'Ignore drafts, auto drafts or trashed posts...' );
			return;
		}

		// quit if post is not exhibition
		if( $source_post->post_type !== $this->settings['post_type']['exhibitions'] && $source_post->post_type !== $this->settings['post_type']['satelite'] ) {
			$this->writeLog( 'Ignore non-exhibitions...' );
			return;
		}		

		// quit if post has no opening date
		if( !get_post_meta( $source_post_id, $this->settings['meta_key']['opening-date'], true ) ) {
			$this->writeLog( 'Ignore posts without opening date...' );
			return;	
		}

		// quit if post is an opening event
		if( get_post_meta( $source_post_id, $this->settings['meta_key_relation']['opening'], true ) ) {
			$this->writeLog( 'Ignore opening events...' );
			return;	
		}		

		$this->writeLog( 'meta key opening date: ' .  $this->settings['meta_key']['opening-date'] );
		if( $date = get_post_meta( $source_post_id, $this->settings['meta_key']['opening-date'], true ) ) {
			$this->writeLog( 'opening date: ' . $date );
		}


		$target_post_id = get_post_meta( $source_post_id, $this->settings['meta_key_relation']['exhibition'], true );

		// create opening post
		if( !$target_post_id ) {
			$target_post_id = $this->createPost( $source_post_id, $source_post );
		} 

		$this->writeLog( $target_post_id );


		/**
		 * Set title 
		 */
		$this->setPostTitle( $source_post_id, $target_post_id );


		/**
		 * Set content
		 */
		$this->setPostContent( $source_post_id, $target_post_id );


		/**
		 * Set thumbnail image
		 */
		$this->setPostThumbnail( $source_post_id, $target_post_id );


		/**
		 * Set tags, cetagories and custom taxonomies
		 */
		$this->setTaxonomies( $source_post_id, $target_post_id );


		/**
		 * Set tags, cetagories and custom taxonomies
		 */
		$this->setOpeningMeta( $source_post_id, $target_post_id );


		/**
		 * Set relations
		 */
		update_post_meta( $source_post_id, $this->settings['meta_key_relation']['exhibition'], $target_post_id );
		update_post_meta( $target_post_id, $this->settings['meta_key_relation']['opening'], $source_post_id );


		/**
		 * Set MultilingualPress relations
		 */
		$this->setMultilingualPressrelations( $source_post_id, $target_post_id );


		/**
		 * Call custom action: save post
		 */
		do_action( 
			'agbo/save_post',
			$source_post_id,
			$target_post_id
		);				
	}


	/**
	 * Create new post in remote site
	 * @param int $source_post_id post ID of source post
	 * @param array $source_post original post
	 * @return int $target_post_id post ID of target post
	 */
	public function createPost( $source_post_id, $source_post ) {
		$this->writeLog( 'createPost()' );
		$this->writeLog( 'current site id: ' );
		$this->writeLog( get_current_blog_id() );

		$post_data = array(
			'ID' 				=> 0,
			'post_title' 		=> $source_post->post_title,
			'post_date' 		=> $source_post->post_date,
			'post_modified' 	=> $source_post->post_modified,
			'post_modified_gmt' => $source_post->post_modified_gmt,
			'post_status' 		=> $source_post->post_status,
			'post_author' 		=> $source_post->post_author,
			'post_excerpt'  	=> $source_post->post_excerpt,
			'post_type' 		=> $this->settings['post_type']['events'],
			'post_name' 		=> $source_post->post_name,
			'meta_input' 		=> array(
				'hmnap--ignore' 	=> 1
			)
		);

		if( get_post_type( $source_post_id ) === $this->settings['post_type']['satelite'] ) {
			$post_data['post_type'] = $this->settings['post_type']['satelite'];
		} else {
			$post_data['post_type'] = $this->settings['post_type']['events'];
		}

		// remove save_post action to avoid infinite loop
		remove_action( 'save_post', array( $this, 'savePost' ), 100 );	
		// create post
		$target_post_id = wp_insert_post( $post_data );
		// re-add save_post action
		add_action( 'save_post', array( $this, 'savePost' ), 100, 2 );	


		/**
		 * Call custom action: create post
		 */
		do_action( 
			'agbo/create_post',
			$source_post_id,
			$target_post_id
		);	

		return $target_post_id;
	}


	/**
	 * Set post title of post
	 * @param int $source_post_id post ID of source post
	 * @param int $target_post_id post ID of target post
	 */
	public function setPostTitle( $source_post_id, $target_post_id ) {
		$this->writeLog( 'setPostTitle()' );

		$source_post = get_post( $source_post_id );
		if( !$source_post ) {
			return;
		}

		// title and post name
		$post_data = array(
      		'ID'           => $target_post_id,
      		'post_title'   => $source_post->post_title,
      		'post_name'    => $source_post->post_name
		);

		// remove save_post action to avoid infinite loop
		remove_action( 'save_post', array( $this, 'savePost' ), 100 );
		// update post data
  		wp_update_post( $post_data );
		// re-add save_post action
		add_action( 'save_post', array( $this, 'savePost' ), 100, 2 );

		// subtitle
		if( $value = get_post_meta( $source_post_id, 'wps_subtitle', true ) ) {
			update_post_meta( $target_post_id, 'wps_subtitle', $value );
		} else {
			delete_post_meta( $target_post_id, 'wps_subtitle' );
		}
	}


	/**
	 * Set post content of post
	 * @param int $source_post_id post ID of source post
	 * @param int $target_post_id post ID of target post
	 */
	public function setPostContent( $source_post_id, $target_post_id ) {
		$this->writeLog( 'setPostContent()' );
		
		$source_post = get_post( $source_post_id );
		if( !$source_post ) {
			return;
		}

		$post_data = array(
      		'ID'           => $target_post_id,
      		'post_content' => $source_post->post_content,
      		'post_excerpt' => $source_post->post_excerpt
		);

		// remove save_post action to avoid infinite loop
		remove_action( 'save_post', array( $this, 'savePost' ), 100 );
		// update post data
  		wp_update_post( $post_data );
		// re-add save_post action
		add_action( 'save_post', array( $this, 'savePost' ), 100, 2 );		
	}	


	/**
	 * Set post thumbnail of post
	 * @param int $source_post_id post ID of source post
	 * @param int $target_post_id post ID of target post
	 */
	public function setPostThumbnail( $source_post_id, $target_post_id ) {
		$this->writeLog( 'setPostThumbnail()' );

		// if source post has thumbnail
		if( $source_post_thumbnail_id = get_post_thumbnail_id( $source_post_id ) ) {
			// set thumbnail
			update_post_meta( $target_post_id, '_thumbnail_id', $source_post_thumbnail_id );
		} else {
			// remove thumbnail
			delete_post_meta( $target_post_id, '_thumbnail_id' );			
		}
	}


	/**
	 * Set taxonomies of remote posts
	 * @param int $source_post_id post ID of source post
	 * @param int $target_post_id post ID of target post
	 */
	public function setTaxonomies( $source_post_id, $target_post_id ) {
		$this->writeLog( 'setTaxonomies()' );
		$this->writeLog( $this->settings['taxonomies'] );

		foreach( $this->settings['taxonomies'] as $taxonomy => $terms ) {
			$this->writeLog( $taxonomy );

			foreach( $terms as $term ) {
				if( term_exists( $term, $taxonomy ) ) {
					$term_object = get_term_by( 'slug', $term, $taxonomy );
					$this->writeLog( 'term exists: ' . $term );
					wp_set_post_terms( $target_post_id, $term_object->term_id, $taxonomy, false );					
				}
			}
		}
	}


	/**
	 * Set meta fields of opeing fields
	 * @param int $source_post_id post ID of source post
	 * @param int $target_post_id post ID of target post
	 */
	public function setOpeningMeta( $source_post_id, $target_post_id ) {
		$this->writeLog( 'setOpeningMeta()' );
		$this->writeLog( $source_post_id . ' -> ' . $target_post_id );

		foreach( $this->settings['meta_key'] as $k => $key ) {
			delete_post_meta( $target_post_id, $key );
			if( $values = get_post_meta( $source_post_id, $key, false ) ) {
				foreach( $values as $value ) {
					add_post_meta( $target_post_id, $key, $value );
				}
			}
		}

		// event date
		if( $value = get_post_meta( $source_post_id, $this->settings['meta_key']['opening-date'], true ) ) {
			update_post_meta( $target_post_id, 'event-date--start', $value );
		}
	}	


	/**
	 * Call savePost() on remonte sites in network installations
	 * Background: When using hm-network-auto-post the remote posts are created by 'wp_insert_post()'
	 * that triggers the 'save_post' action we are using here. But at this point the opening data is 
	 * not yet available because meta data is added later. So we hook into a later (custom) action 
	 * and check for the exhibition post type and opening date. Note that we also need to switch 
	 * to the remote site.
	 * @param int $source_site_id site ID of source site
	 * @param int $target_site_id site ID of target site
	 * @param int $source_post_id post ID of source post
	 * @param int $target_post_id post ID of target post
	 */
	public function HMNAPsetMeta( $source_site_id, $target_site_id, $source_post_id, $target_post_id ) {
		$this->writeLog( 'HMNAPsetMeta()' );

		// switch to target site
		switch_to_blog( $target_site_id );

		$this->writeLog( 'post ID: ' . $target_post_id );
		$this->writeLog( 'post type: ' . get_post_type( $target_post_id ) );
		$this->writeLog( 'post title: ' . get_the_title( $target_post_id ) );

		// quit if post is not exhibition
		if( get_post_type( $target_post_id ) !== $this->settings['post_type']['exhibitions'] && get_post_type( $target_post_id ) !== $this->settings['post_type']['satelite'] ) {
			$this->writeLog( 'Ignore non-exhibitions...' );
			// switch back to source site
			restore_current_blog();			
			return;
		}

		// quit if post has no opening date
		if( !get_post_meta( $target_post_id, $this->settings['meta_key']['opening-date'], true ) ) {
			$this->writeLog( 'Ignore posts without opening date...' );
			// switch back to source site
			restore_current_blog();					
			return;	
		}

		// if the post in question has a related opening post
		$this->savePost( $target_post_id, get_post( $target_post_id ) );

		// switch back to source site
		restore_current_blog();
	}


	/**
	 * Set Multilingualpress relations
	 * @param int $source_post_id exhibition post ID
	 * @param int $target_post_id opening post ID
	 */
	public function setMultilingualPressrelations( $source_post_id, $target_post_id ) {
		$this->writeLog( 'HMNAPsetMeta() ' . $source_post_id . '>' . $target_post_id );

		if( !function_exists( 'mlp_get_linked_elements' ) ) {
			return;
		}

		$sites = wp_get_sites();
		$source_site_id = get_current_blog_id();

		// get existing MLP relations
		// Array( [site_id] => [post_id] ) including source site
		$source_relations = mlp_get_linked_elements( $source_post_id, '', $source_site_id );
		$target_relations = mlp_get_linked_elements( $target_post_id, '', $source_site_id );

		// each site... 
		foreach( $sites as $site ) {
			$remote_site_id = $site['blog_id'];

			// skip current site
			if( $remote_site_id == $source_site_id ) {
				continue;
			}

			// skip if opening is alredy related
			if( array_key_exists( $remote_site_id, $target_relations ) ) {
				continue;
			}			

			// skip if exibition has no relations
			if( !array_key_exists( $remote_site_id, $source_relations ) ) {
				continue;
			}

			$remote_source_post_id = $source_relations[$remote_site_id];

			// switch to remote site
			switch_to_blog( $remote_site_id );												
			
			// get opening of remote exhibition				
			if( $remote_target_post_id = get_post_meta( $remote_source_post_id, $this->settings['meta_key_relation']['exhibition'], true ) ) {
				if( $this->mpl_api_cache ) {            
					$relations = $this->mpl_api_cache->get( 'content_relations' );

					$relations->set_relation(
						$source_site_id,
						$remote_site_id,
						$target_post_id,
						$remote_target_post_id
					);

					$this->writeLog( array(
						$source_site_id,
						$remote_site_id,
						$target_post_id,
						$remote_target_post_id
					) );					
				}
			}
			// switch back to source site
			restore_current_blog();				
		}		
	}


	/**
	 * Write log if WP_DEBUG is active
	 * @param  string|array $log 
	 */
	public function writeLog( $log )  {
		if( true === WP_DEBUG ) {
			if( is_array( $log ) || is_object( $log ) ) {
				error_log( 'AGBO: ' . print_r( $log, true ) . "\n", 3, trailingslashit( ABSPATH ) . 'wp-content/debuglog.log' );
			} else {
				error_log( 'AGBO: ' . $log . "\n", 3, trailingslashit( ABSPATH ) . 'wp-content/debuglog.log' );
			}
		}
	}
}

new AGBOpenings();